#!/bin/bash
set -e

BUILD_DIR="build"
ROOTFS_DIR="rootfs"
readonly USAGE_STRING="Usage: ./${0##*/} [-o|c|r] cross-compile-prefix kernelconfig [dts]"
KERNEL_IMAGE=$(realpath "linux/arch/riscv/boot/Image")
readonly KERNEL_IMAGE

parse_arguments(){
	# Parse arguments
	args="$(getopt -o co:r: --name "$0" -- "$@")"
	eval set -- "$args"

	while true
	do
		case "$1" in
			-o)
				BUILD_DIR="$2"
				shift 2
				;;
			-c)
				COMPRESS_INITRAMFS=1
				shift
				;;
			-r)
				ROOTFS_DIR="$2"
				shift 2
				;;
			--)
				shift
				break
				;;
			*)
				echo "Unrecognized option: $1"
				echo "$USAGE_STRING"
				exit 1
				;;
		esac
	done

	readonly BUILD_DIR
	readonly COMPRESS_INITRAMFS
	readonly ROOTFS_DIR

	if [ "$#" != 2 -a "$#" != 3 ]; then
		echo "$USAGE_STRING"
		exit 1
	fi

	readonly CROSS_COMPILE="$1"
	readonly KERNEL_CONFIG="$2"
	if [ ! -f "$KERNEL_CONFIG" ]; then
		echo "config \"$KERNEL_CONFIG\" not found"
		exit 1
	fi

	if [ ! -f "${CROSS_COMPILE}gcc" ]; then
		echo "Invalid riscv toolchain prefix"
		exit 1
	fi

	if [ -n "$3" ]; then
		readonly DEVICE_TREE_STRING="$3"
	fi
}

create_initramfs(){

	local rootfs_name=$(basename "$ROOTFS_DIR")
	if [ -z $COMPRESS_INITRAMFS ]; then
		local initramfs_name="initramfs_$rootfs_name.cpio"
	else
		local initramfs_name="initramfs_$rootfs_name.cpio.gz"
	fi

	echo "Creating ${initramfs_name} archive..."
	INITRAMFS="$(realpath "$BUILD_DIR")/$initramfs_name"
	readonly INITRAMFS
	# Remove any existing archive
	sudo rm -f "$INITRAMFS"

	# Wrap this in 'bash -c' so that the redirection has root permissions when executing the script with sudo
	if [ -z $COMPRESS_INITRAMFS ]; then
		sudo bash -c "cd $ROOTFS_DIR && find . | cpio --create --format=newc > $INITRAMFS"
	else
		sudo bash -c "cd $ROOTFS_DIR && find . | cpio --create --format=newc | gzip > $INITRAMFS"
	fi
	echo "${initramfs_name} size: $(stat -c %s "$INITRAMFS")"
}

build_kernel(){
	# Put the defconfig file into the expected directory
	cp -f "$KERNEL_CONFIG" "linux/arch/riscv/configs/enzian_defconfig"
	# Substitute the initramfs path in the defconfig file
	sed -i "/CONFIG_INITRAMFS_SOURCE=/c\CONFIG_INITRAMFS_SOURCE=\"${INITRAMFS}\"" "linux/arch/riscv/configs/enzian_defconfig"

	# Generate a config file and compile the kernel
	echo "Compiling kernel with initramfs archive ${INITRAMFS} ..."

	local kernel_flags
	kernel_flags="--jobs=$(nproc) ARCH=riscv CROSS_COMPILE=$CROSS_COMPILE"
	sudo make -C linux $kernel_flags mrproper # Start from scratch every time
	make -C linux $kernel_flags enzian_defconfig
	make -C linux $kernel_flags Image
	echo "Kernel image size: $(stat -c %s "$KERNEL_IMAGE")"

	cp "$KERNEL_IMAGE" "$BUILD_DIR/Image_$(basename "$KERNEL_CONFIG")"
	generated_kernel_config="linux/.config"
	cp $generated_kernel_config "$BUILD_DIR/$(basename "$KERNEL_CONFIG")"
}

build_opensbi(){
	echo "Compiling OpenSBI..."

	# Determine FW_PAYLOAD_FDT_ADDR - CAREFUL: This only works with default FW_TEXT_START and FW_PAYLOAD_OFFSET values
	# FW_PAYLOAD_FDT_ADDR = FW_TEXT_START + FW_PAYLOAD_OFFSET + PAYLOAD_SIZE
	payload_size=$(printf '%#X' "$(stat -c %s "$KERNEL_IMAGE")")
	# Address needs to be aligned to 2MB for 64bit, 4MB for 32bit (align = boundary - 1)
	align=0x1FFFFF
	fw_payload_fdt_addr=$(printf '%#X' $(((0x80000000 + 0x200000 + payload_size + align) & ~(align))))
	echo "FW_PAYLOAD_FDT_ADDR=$fw_payload_fdt_addr"

	sbi_flags="CROSS_COMPILE=$CROSS_COMPILE PLATFORM=generic FW_PAYLOAD_PATH=$KERNEL_IMAGE FW_PAYLOAD_FDT_ADDR=$fw_payload_fdt_addr PLATFORM_RISCV_ISA=rv64imafdc FW_JUMP=n FW_DYNAMIC=n BUILD_INFO=y PLATFORM_RISCV_XLEN=64"

	final_bin="$BUILD_DIR/payload_$(basename "$ROOTFS_DIR")_$(basename "$KERNEL_CONFIG")"

	# If the parameter is set, compile the given device tree and include it in the binary
	if [ -n "$DEVICE_TREE_STRING" ]; then
		dts_name=$(basename "$DEVICE_TREE_STRING")
		dts_name=${dts_name/.dts/}
		dtb_opensbi=$(realpath "$BUILD_DIR/$dts_name.dtb")

		dtc -o "$dtb_opensbi" "$DEVICE_TREE_STRING"

		sbi_flags=$sbi_flags" FW_FDT_PATH=$dtb_opensbi"
		echo "Compiling SBI with embedded FDT $DEVICE_TREE_STRING"

		final_bin="${final_bin}_fdt_$dts_name"
	fi

	echo "SBI FLAGS: $sbi_flags"
	make -C opensbi clean #Opensbi doesn't recompile if flags change otherwise
	make -C opensbi $sbi_flags

	final_bin="$final_bin.bin"
	cp "opensbi/build/platform/generic/firmware/fw_payload.bin" "$final_bin"
	echo "Final binary:  $final_bin"
	echo "Size: $(stat -c %s "$final_bin")"
}

main(){

parse_arguments "$@"
# Setup directories
mkdir -p "$BUILD_DIR"

# Disable building the kernel with NO_KERNEL=1 
# - mixing flags and arguments like this seems like terrible practice, but it works for now
# - this whole thing should use 'make' anyways
if [ -z ${NO_KERNEL+x} ]; then 
	# Turn the chroot into an archive we can use as the initramfs
	create_initramfs
	# Build the kernel
	build_kernel
else
	echo "NO_KERNEL is set, skipping kernel build"
fi

# Build OpenSBI with the kernel as payload
build_opensbi
}

main "$@"
