#!/bin/bash

run_image(){
	qemu-system-riscv64 \
	-nographic \
	--machine virt \
	-m 8G \
	-kernel $1 \
	-append "loglevel=7"

}

run_opensbi(){
	qemu-system-riscv64 \
	-nographic \
	--machine virt \
	-m 8G \
	-bios $1
}

PS3="Select a test: "
select func in run_image run_opensbi
do
	$func "$1" && exit
done
