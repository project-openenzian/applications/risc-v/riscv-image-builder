/dts-v1/;

/ {
	#address-cells = <2>;
	#size-cells = <2>;
	compatible = "freechips,rocketchip-unknown-dev";
	model = "freechips,rocketchip-unknown";
	L21: aliases {
		serial0 = &L14;
	};
	L20: cpus {
		#address-cells = <1>;
		#size-cells = <0>;
		timebase-frequency = <1000000>;
		L5: cpu@0 {
			clock-frequency = <0>;
			compatible = "ucb-bar,boom0", "riscv";
			d-cache-block-size = <64>;
			d-cache-sets = <64>;
			d-cache-size = <16384>;
			d-tlb-sets = <1>;
			d-tlb-size = <8>;
			device_type = "cpu";
			hardware-exec-breakpoint-count = <0>;
			i-cache-block-size = <64>;
			i-cache-sets = <64>;
			i-cache-size = <16384>;
			i-tlb-sets = <1>;
			i-tlb-size = <32>;
			mmu-type = "riscv,sv39";
			next-level-cache = <&L3>;
			reg = <0x0>;
			riscv,isa = "rv64imafdc";
			riscv,pmpgranularity = <4>;
			riscv,pmpregions = <8>;
			status = "okay";
			timebase-frequency = <1000000>;
			tlb-split;
			L4: interrupt-controller {
				#interrupt-cells = <1>;
				compatible = "riscv,cpu-intc";
				interrupt-controller;
			};
		};
	};
	L9: memory@80000000 {
		device_type = "memory";
		reg = <0x0 0x80000000 0x4 0x0>;
	};
	L19: soc {
		#address-cells = <2>;
		#size-cells = <2>;
		compatible = "freechips,rocketchip-unknown-soc", "simple-bus";
		ranges;
		pmu {
					compatible 			= "riscv,pmu";
					riscv,event-to-mhpmevent =
					/* SBI_PMU_HW_CACHE_MISSES -> Instruction cache miss | Data cache miss or memory-mapped I/O access */
									 <0x00004 0x00000000 0x0302>,
					/* L1D_READ_MISS -> Data cache miss or memory-mapped I/O access */
									 <0x10001 0x00000000 0x0202>,
					/* L1D_WRITE_ACCESS -> Data cache write-back */
									 <0x10002 0x00000000 0x0402>,
					/* L1I_READ_ACCESS -> Instruction cache miss */
									 <0x10009 0x00000000 0x0102>,
					/* DTLB_READ_MISS -> Data TLB miss */
									 <0x10019 0x00000000 0x1002>,
					/* ITLB_READ_MISS-> Instruction TLB miss */
									 <0x10021 0x00000000 0x0802>;
					riscv,event-to-mhpmcounters = 
											<0x00003 0x00006 0xfffffff8>,
											<0x10001 0x10002 0xfffffff8>,
											<0x10009 0x10009 0xfffffff8>,
											<0x10019 0x10019 0xfffffff8>,
											<0x10021 0x10021 0xfffffff8>;
					riscv,raw-event-to-mhpmcounters = 
					/* Rocket-chip uses a fully-associative mapping, i.e. any counter can be used to monitor any event */
					/* Instruction Commit Events - Set 0x0 , Bits 8 */
										<0x0 0x0 0xffffffff 0xfffffeff 0xfffffff8>,
					/* Microarchitectural Events - Set 0x1 , Bits 9 */
										<0x0 0x1 0xffffffff 0xfffffdff 0xfffffff8>,
					/* Memory System Events - Set 0x2 , Bits 8-12 */
										<0x0 0x2 0xffffffff 0xffffe0ff 0xfffffff8>;
		};
		L13: boot-address-reg@4000 {
			reg = <0x0 0x4000 0x0 0x1000>;
			reg-names = "control";
		};
		L3: cache-controller@2010000 {
			cache-block-size = <64>;
			cache-level = <2>;
			cache-sets = <1024>;
			cache-size = <524288>;
			cache-unified;
			compatible = "sifive,inclusivecache0", "cache";
			next-level-cache = <&L9>;
			reg = <0x0 0x2010000 0x0 0x1000>;
			reg-names = "control";
			sifive,mshr-count = <7>;
		};
		L7: clint@2000000 {
			compatible = "riscv,clint0";
			interrupts-extended = <&L4 3 &L4 7>;
			reg = <0x0 0x2000000 0x0 0x10000>;
			reg-names = "control";
		};
		L15: clock-gater@100000 {
			reg = <0x0 0x100000 0x0 0x1000>;
			reg-names = "control";
		};
		L1: error-device@3000 {
			compatible = "sifive,error0";
			reg = <0x0 0x3000 0x0 0x1000>;
		};
		L6: interrupt-controller@c000000 {
			#interrupt-cells = <1>;
			compatible = "riscv,plic0";
			interrupt-controller;
			interrupts-extended = <&L4 11 &L4 9>;
			reg = <0x0 0xc000000 0x0 0x4000000>;
			reg-names = "control";
			riscv,max-priority = <1>;
			riscv,ndev = <1>;
		};
		L12: rom@1000000 {
			compatible = "sifive,rom0";
			reg = <0x0 0x1000000 0x0 0x400000>;
			reg-names = "mem";
		};
		L14: serial@64000000 {
			clocks = <&L0>;
			compatible = "sifive,uart0";
			interrupt-parent = <&L6>;
			interrupts = <1>;
			reg = <0x0 0x64000000 0x0 0x1000>;
			reg-names = "control";
		};
		L2: subsystem_mbus_clock {
			#clock-cells = <0>;
			clock-frequency = <100000000>;
			clock-output-names = "subsystem_mbus_clock";
			compatible = "fixed-clock";
		};
		L0: subsystem_pbus_clock {
			#clock-cells = <0>;
			clock-frequency = <100000000>;
			clock-output-names = "subsystem_pbus_clock";
			compatible = "fixed-clock";
		};
		L16: tile-reset-setter@110000 {
			reg = <0x0 0x110000 0x0 0x1000>;
			reg-names = "control";
		};
	};
};
