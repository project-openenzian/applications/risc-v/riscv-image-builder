#!/bin/bash
cd rootfs_overlay/home/enzian/faas_uarch_experiments
python3 run_faas_benchmark_single.py aditya riscv64 rocket --generate $(realpath aditya.sh) --basedir="/home/enzian/faas_uarch_experiments"
python3 run_faas_benchmark_single.py python_micros riscv64 rocket --generate $(realpath python_micros.sh) --basedir="/home/enzian/faas_uarch_experiments"
python3 run_faas_benchmark_single.py rust_faas_functions riscv64 rocket --generate $(realpath rust_faas_functions.sh) --basedir="/home/enzian/faas_uarch_experiments"
python3 run_faas_benchmark_single.py rust_faas_functions riscv64 rocket --generate $(realpath rust_faas_functions.sh) --basedir="/home/enzian/faas_uarch_experiments"
python3 run_faas_benchmark_single.py coremark riscv64 rocket --generate $(realpath coremark.sh) --basedir="/home/enzian/faas_uarch_experiments"
python3 run_faas_benchmark_single.py new_faas_benchmark riscv64 rocket --generate $(realpath new_faas_benchmark.sh) --basedir="/home/enzian/faas_uarch_experiments"
