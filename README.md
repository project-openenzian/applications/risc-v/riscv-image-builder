# Installing the dependencies

`sudo apt-get install mmdebstrap debian-ports-archive-keyring qemu-user qemu-user-binfmt qemu-user-static qemu-system-misc binfmt-support`

# Add debian-ports public key

Then, run 

`wget -O - https://www.ports.debian.org/archive_2023.key | apt-key add -`


# Building the image
To compile the kernel, a RISC-V toolchain is needed. See [RISC-V GNU Compiler Toolchain](https://github.com/riscv-collab/riscv-gnu-toolchain) for instructions.

In the following commands, `CROSS_COMPILE` is the prefix to the installed binaries of the toolchain, .e.g `/opt/riscv/bin/riscv64-unknown-linux-gnu-`.

Once all the dependencies are installed, build the root filesystem using `./mk-initramfs.sh CROSS_COMPILE` - the default output directory is `rootfs`.
If you want to include additional files, you can either modify the script or place them in `rootfs-overlay` before building the root filesystem.
To update an existing rootfs with new files in `rootfs-overlay` use `./mk-initramfs.sh --update`

Build the final image with: `./mk-image.sh CROSS_COMPILE defconfig`

# Issues & Testing
Test the resulting OpenSBI or Linux kernel image with: `./qemu-test.sh image`

- It is normal to see every kernel message twice, because hvc0 and ttyS0 share the same console on the QEMU `virt` machine
- There are some issues with older versions of QEMU that are available through `apt` (notably 6.2), so you might want to build a newer version from source. (Version 7.2 works).
- Kernel images past a certain size (~1GB compressed) do not boot on either QEMU or the RISCV SoC
