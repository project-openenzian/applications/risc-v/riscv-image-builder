#!/bin/bash
# Create a debian-based rootfs using mmdebstrap
set -e

CHROOT_DIR="rootfs"
readonly USAGE_STRING="Usage: $0 [-o/--outdir chroot-directory] [-u/--update] cross-compile-prefix"

readonly DEB_VARIANT="standard"
readonly PYTHON_PKGS="python3-gnupg python3-pycryptodome python3-opencv"
readonly APR_PKGS="build-essential libapr1-dev libaprutil1-dev libpcre3-dev"
# PYTHON_PKGS="$PYTHON_PKGS python3-numpy python3-pyaes python3-six python3-chameleon"
readonly DOCKER_PKGS="docker.io"
APT_PKGS=("$PYTHON_PKGS $APR_PKGS")

readonly SID_ARCHIVE="deb http://deb.debian.org/debian-ports sid main"
readonly MIRROR="deb http://deb.debian.org/debian-ports unstable main"
readonly MIRROR2="deb http://deb.debian.org/debian-ports unreleased main"

UPDATE_ONLY=0

parse_arguments(){

	# Parse arguments
	args="$(getopt -o uo: -l update,outdir: --name "$0" -- "$@")"
	eval set -- "$args"

	while true
	do
		case "$1" in
			-o|--outdir)
				CHROOT_DIR="$2"
				shift 2
				;;
			-u|--update)
				UPDATE_ONLY=1
				shift
				break
				;;
			--)
				shift
				break
				;;
			*)
				echo "Unrecognized option: $1"
				echo "$USAGE_STRING"
				exit 1
				;;
		esac
	done

	if [ "$UPDATE_ONLY" -eq 0 ]; then
		if [ "$#" != 1 ]; then
			echo "$USAGE_STRING"
			exit 1
		fi

		CROSS_COMPILE="$1"

		if [ ! -f "${CROSS_COMPILE}gcc" ]; then
			echo "Invalid riscv toolchain prefix"
			exit 1
		fi
	fi

	readonly CROSS_COMPILE
	readonly CHROOT_DIR
	readonly UPDATE_ONLY

}


create_rootfs(){
	mkdir -p "$CHROOT_DIR"

	sudo mmdebstrap  \
		--architectures=riscv64 \
		--variant="$DEB_VARIANT" \
		--include=debian-ports-archive-keyring,adduser,sudo \
		--dpkgopt="dpkg-opts" \
		sid \
		"$CHROOT_DIR" \
		"$SID_ARCHIVE" "$MIRROR" "$MIRROR2" 

	# Create and set passwords for root and user enzian
	sudo chroot "$CHROOT_DIR" adduser --gecos "Enzian user,,," --disabled-password enzian
	echo "root:root" | sudo chroot "$CHROOT_DIR" chpasswd
	echo "enzian:enzian" | sudo chroot "$CHROOT_DIR" chpasswd
	sudo chroot "$CHROOT_DIR" usermod -aG sudo enzian

	# Update APT database
	sudo chroot "$CHROOT_DIR" apt-get update
	# Install additional packages
	sudo chroot "$CHROOT_DIR" apt-get install -y ${APT_PKGS[@]}
	# Clear cache
	sudo chroot "$CHROOT_DIR" apt-get clean

	# Compile and install perf
	# We need sudo here as the rootfs we install to requires root priviliges
	perf_flags="ARCH=riscv CROSS_COMPILE=$CROSS_COMPILE FEATURE_DUMP=$(realpath "perf-features") DESTDIR=$(realpath "$CHROOT_DIR") NO_LIBELF=1"
	sudo make -C linux/tools/perf $perf_flags clean
	sudo make -C linux/tools/perf $perf_flags install
}

update_overlay(){
	# Copy the rootfs overlay over, preserving permissions
	sudo cp -rp rootfs_overlay/* "$CHROOT_DIR/"
	# Fix permissions
	sudo chroot "$CHROOT_DIR" chown -R "enzian:enzian" "/home/enzian"
}

main(){

	parse_arguments "$@"

	# Only create the rootfs if UPDATE_ONLY=0
	if [ "$UPDATE_ONLY" -eq 0 ]; then
		echo "Creating rootfs in $CHROOT_DIR"
		create_rootfs
	else
		echo "UPDATE_ONLY=1, only copying rootfs_overlay"
	fi

	update_overlay
}

main "$@"
